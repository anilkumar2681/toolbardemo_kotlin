package com.anil.toolbardemo_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Context
import android.view.LayoutInflater
import android.widget.TextView


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar!!.title = "Toolbar"
        actionBar.elevation = 4.0F
        actionBar.setDisplayShowHomeEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater=menuInflater
        inflater.inflate(R.menu.toolbar_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.manu_cut->{
                showToast("You clicked CUT menu Option")
            }
            R.id.menu_copy->{
               showToast("You clicked COPY menu Option")
            }
            R.id.menu_paste->{
               showToast("You clicked PASTE menu Option")
            }
            R.id.menu_new->{
               showToast("You clicked NEW menu Option")
            }
        }
        return super.onOptionsItemSelected(item)
    }


    fun showToast(msg:String){

        val toast = Toast(this)
        var textView:TextView?=null
        toast.duration = Toast.LENGTH_LONG
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.custom_toast_layout, null)
        textView = view.findViewById(R.id.toastMsg)
        textView.text=msg
        toast.view = view
        toast.show()

    }
}
